/*
 * fpackutil.h
 *
 *  Created on: 25 Mar 2019
 *      Author: rafa
 */

#ifndef FPACKUTIL_H_
#define FPACKUTIL_H_

  #include "my_stream.h"

  int fp_noop (void);
  void fp_abort_output (fitsfile *infptr, fitsfile *outfptr, int stat);
  int fp_version (void);
  int fp_access (char *filename);
  int fp_tmpnam(char *suffix, char *rootname, char *tmpnam);
  int fp_init (fpstate *fpptr);
  int fp_list (int argc, char *argv[], fpstate fpvar);
  int fp_info_hdu (fitsfile *infptr);
  int fp_preflight (int argc, char *argv[], int unpack, fpstate *fpptr);
  int fp_loop (int argc, char *argv[], int unpack, fpstate fpvar);
  int fp_pack (char *infits, char *outfits, fpstate fpvar, int *islossless);
  int fp_unpack (char *infits, char *outfits, fpstate fpvar);
  int fp_test (char *infits, char *outfits, char *outfits2, fpstate fpvar);
  int fp_pack_hdu (fitsfile *infptr, fitsfile *outfptr, fpstate fpvar,
     int *islossless, int *status);
  int fp_unpack_hdu (fitsfile *infptr, fitsfile *outfptr, fpstate fpvar, int *status);
  int fits_read_image_speed (fitsfile *infptr, float *whole_elapse,
      float *whole_cpu, float *row_elapse, float *row_cpu, int *status);
  int fp_test_hdu (fitsfile *infptr, fitsfile *outfptr, fitsfile *outfptr2,
  	fpstate fpvar, int *status);
  int fp_test_table (fitsfile *infptr, fitsfile *outfptr, fitsfile *outfptr2,
  	fpstate fpvar, int *status);
  int marktime(int *status);
  int gettime(float *elapse, float *elapscpu, int *status);
  int fp_i2stat(fitsfile *infptr, int naxis, long *naxes, imgstats *imagestats, int *status);
  int fp_i4stat(fitsfile *infptr, int naxis, long *naxes, imgstats *imagestats, int *status);
  int fp_r4stat(fitsfile *infptr, int naxis, long *naxes, imgstats *imagestats, int *status);
  int fp_i2rescale(fitsfile *infptr, int naxis, long *naxes, double rescale,
      fitsfile *outfptr, int *status);
  int fp_i4rescale(fitsfile *infptr, int naxis, long *naxes, double rescale,
      fitsfile *outfptr, int *status);
  void abort_fpack(int sig);

  int fp_msg (char *msg);


#endif /* FPACKUTIL_H_ */
