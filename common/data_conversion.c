/*
 * data_conversion.c
 *
 *  Created on: 12 jun. 2019
 *      Author: rafa
 */
#include <stdint.h>
//----------------------------------------------------------------------------
void swap_16_byte (uint8_t *in, uint64_t size, uint8_t * out)
{
  uint64_t i = 0;
  for (i = 0 ; i< size; ++i){
	  *(out+1) = *in++;
	  *out = *in++;
	  out += 2;
  }
}

//-----------------------------------------------------------------------------
void swap_32_byte(uint8_t *in, uint64_t size, uint8_t * out){

  uint64_t i = 0;
  for (i = 0 ; i< size; ++i){
	*(out+3) = *in++;
	*(out+2) = *in++;
	*(out+1) = *in++;
	*out = *in++;
	out += 4;
  }
}
//-----------------------------------------------------------------------------
void swap_64_byte(uint8_t *in, uint64_t size, uint8_t * out){

  uint64_t i = 0;
  for (i = 0 ; i< size; ++i){
	*(out+7) = *in++;
	*(out+6) = *in++;
	*(out+5) = *in++;
	*(out+4) = *in++;
	*(out+3) = *in++;
	*(out+2) = *in++;
	*(out+1) = *in++;
	*out = *in++;
	out += 8;
 }
}
//-----------------------------------------------------------------------------
void array_byte_to_short(uint8_t * in, uint64_t size, uint16_t * out){
	swap_16_byte(in, size/2, (uint8_t *) out);
}

//-----------------------------------------------------------------------------
void array_short_to_byte(uint16_t * in, uint64_t size, uint8_t * out){
 	swap_16_byte((uint8_t *)in, size, out);
}

//-----------------------------------------------------------------------------
void array_byte_to_int(uint8_t * in, uint64_t size, uint32_t * out){
	swap_32_byte(in, size/4, (uint8_t *) out);
}

//-----------------------------------------------------------------------------
void array_int_to_byte(uint32_t * in, uint64_t size, uint8_t * out){
 	swap_32_byte((uint8_t *)in, size, out);
}
//-----------------------------------------------------------------------------
void array_byte_to_long(uint8_t * in, uint64_t size, uint64_t * out){
	swap_64_byte(in, size/8, (uint8_t *) out);
}

//-----------------------------------------------------------------------------
void array_long_to_byte(uint64_t * in, uint64_t size, uint8_t * out){
 	swap_64_byte((uint8_t *)in, size, out);
}

//-----------------------------------------------------------------------------
void array_byte_to_float(uint8_t * in, uint64_t size, float * out){
	swap_32_byte(in, size/4, (uint8_t *) out);
}

//-----------------------------------------------------------------------------
void array_float_to_byte(float * in, uint64_t size, uint8_t * out){
	swap_32_byte((uint8_t *)in, size, out);
}

//-----------------------------------------------------------------------------
void array_byte_to_double(uint8_t * in, uint64_t size, double * out){
	swap_64_byte(in, size/8, (uint8_t *) out);
}

//-----------------------------------------------------------------------------
void array_double_to_byte(double * in, uint64_t size, uint8_t * out){
	swap_64_byte((uint8_t *)in, size, out);
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//End ofc file "coordinate_conversion.c"
//-----------------------------------------------------------------------------
