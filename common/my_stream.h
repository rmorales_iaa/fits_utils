/*
 * my_header.h
 *
 *  Created on: 26 Feb 2019
 *      Author: rafa
 */

#ifndef CONTEXT_MY_STREAM_H_
#define CONTEXT_MY_STREAM_H_

    //-------------------------------------------------------------------------
    #include <stdio.h>
    //-------------------------------------------------------------------------
	typedef struct my_stream {

		//flags
		char isOpen;
		char free_data_when_close;

		//storage
		size_t pos;
		size_t size;
		char * data;

	} my_stream_type;

    //-------------------------------------------------------------------------

	//open and close
	my_stream_type * my_stream_init_with_file(char * s);
	my_stream_type * my_stream_init_with_buffer(char * b, size_t b_size);
	void my_stream_close(my_stream_type * s);

	//fstream operations
	size_t my_stream_fread(void *ptr, size_t size, size_t nmemb, FILE *f);
	size_t my_stream_ftell(FILE *f);
	int my_stream_fseek(FILE *f, long int offset, int whence);
	void my_stream_rewind(FILE * f);

	//-------------------------------------------------------------------------
#endif /* CONTEXT_MY_STREAM_H_ */
