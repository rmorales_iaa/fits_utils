/*
 * my_util.c
 *
 *  Created on: 3 Apr 2019
 *      Author: rafa
 */
/*--------------------------------------------------------------------------*/
#include <ctype.h>
#include <stddef.h>
/*--------------------------------------------------------------------------*/
#include "fitsio.h"
#include "fpack.h"
#include "fpackutil.h"
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/* fp_unpack assumes the output file does not exist
 */

int my_fp_unpack (my_stream_type * in_stream, my_stream_type * out_stream, fpstate * fpvar)
{
	fitsfile *infptr, *outfptr;
	int stat=0, hdutype, extnum, single = 0;
	char *loc, *hduloc=NULL, hduname[SZ_STR];

	size_t input_stream_byte_size = in_stream->size;
	fits_open_memfile (&infptr, "NO_NAME", READONLY, (void**)&in_stream->data, &input_stream_byte_size, 0, &realloc, &stat);

	if (infptr == NULL){
		printf("\n Error managing memory in the input stream");
		return -900;
	}

	size_t output_stream_byte_size = out_stream->size;
	fits_create_memfile(&outfptr, (void**)&out_stream->data, &output_stream_byte_size, 0, &realloc, &stat);

	if (outfptr == NULL){
		printf("\n Error managing memory in the output stream");
		return -901;
	}

	if (stat) {
		fp_abort_output(infptr, outfptr, stat);
	}

	if (fpvar->extname[0]) {  /* unpack a list of HDUs? */

		/* move to the first HDU in the list */
		hduloc = fpvar->extname;
		loc = strchr(hduloc, ','); /* look for 'comma' delimiter between names */

		if (loc)
			*loc = '\0';  /* terminate the first name in the string */

		strcpy(hduname, hduloc);  /* copy the first name into temporary string */

		if (loc)
			hduloc = loc + 1;  /* advance to the beginning of the next name, if any */
		else {
			hduloc += strlen(hduname);  /* end of the list */
			single = 1;  /* only 1 HDU is being unpacked */
		}

		if (isdigit( (int) hduname[0]) ) {
			extnum = strtol(hduname, &loc, 10); /* read the string as an integer */

			/* check for junk following the integer */
			if (*loc == '\0' )  /* no junk, so move to this HDU number (+1) */
			{
				fits_movabs_hdu(infptr, extnum + 1, &hdutype, &stat);  /* move to HDU number */
				if (hdutype != IMAGE_HDU)
					stat = NOT_IMAGE;
			} else {  /* the string is not an integer, so must be the column name */
				hdutype = IMAGE_HDU;
				fits_movnam_hdu(infptr, hdutype, hduname, 0, &stat);
			}
		}
		else
		{
			/* move to the named image extension */
			hdutype = IMAGE_HDU;
			fits_movnam_hdu(infptr, hdutype, hduname, 0, &stat);
		}
	}

	if (stat) {
		fp_msg ("Unable to find and move to extension '");
		fp_msg(hduname);
		fp_msg("'\n");
		fp_abort_output(infptr, outfptr, stat);
	}

	while (! stat) {

		if (single)
			stat = -1;  /* special status flag to force output primary array */

		fp_unpack_hdu (infptr, outfptr, *fpvar, &stat);

		if (fpvar->do_checksums) {
			fits_write_chksum (outfptr, &stat);
		}

		/* move to the next HDU */
		if (fpvar->extname[0]) {  /* unpack a list of HDUs? */

			if (!(*hduloc)) {
				stat = END_OF_FILE;  /* we reached the end of the list */
			} else {
				/* parse the next HDU name and move to it */
				loc = strchr(hduloc, ',');

				if (loc)         /* look for 'comma' delimiter between names */
					*loc = '\0';  /* terminate the first name in the string */

				strcpy(hduname, hduloc);  /* copy the next name into temporary string */

				if (loc)
					hduloc = loc + 1;  /* advance to the beginning of the next name, if any */
				else
					*hduloc = '\0';  /* end of the list */

				if (isdigit( (int) hduname[0]) ) {
					extnum = strtol(hduname, &loc, 10); /* read the string as an integer */

					/* check for junk following the integer */
					if (*loc == '\0' )   /* no junk, so move to this HDU number (+1) */
					{
						fits_movabs_hdu(infptr, extnum + 1, &hdutype, &stat);  /* move to HDU number */
						if (hdutype != IMAGE_HDU)
							stat = NOT_IMAGE;

					} else {  /* the string is not an integer, so must be the column name */
						hdutype = IMAGE_HDU;
						fits_movnam_hdu(infptr, hdutype, hduname, 0, &stat);
					}

				} else {
					/* move to the named image extension */
					hdutype = IMAGE_HDU;
					fits_movnam_hdu(infptr, hdutype, hduname, 0, &stat);
				}

				if (stat) {
					fp_msg ("Unable to find and move to extension '");
					fp_msg(hduname);
					fp_msg("'\n");
				}
			}
		} else {
			/* increment to the next HDU */
			fits_movrel_hdu (infptr, 1, NULL, &stat);
		}
	}

	if (stat == END_OF_FILE) stat = 0;

	/* set checksum for case of newly created primary HDU
	 */
	if (fpvar->do_checksums) {
		fits_movabs_hdu (outfptr, 1, NULL, &stat);
		fits_write_chksum (outfptr, &stat);
	}


	if (stat) {
		fp_abort_output(infptr, outfptr, stat);
	}

	fits_close_file (outfptr, &stat);
	fits_close_file (infptr, &stat);

	return(0);
}
/*--------------------------------------------------------------------------*/
