/*
 * data_conversion.h
 *
 *  Created on: 12 jun. 2019
 *      Author: rafa
 */

//-----------------------------------------------------------------------------
#ifndef DATA_CONVERSION_H_
#define DATA_CONVERSION_H_
//-----------------------------------------------------------------------------

	#include <stdint.h>
	//-------------------------------------------------------------------------
	 void array_byte_to_short(uint8_t * in, uint64_t size, uint16_t * out);
	 void array_short_to_byte(uint16_t * in, uint64_t size, uint8_t * out);
	 //-----------------------------------------------------------------------------
	 void array_byte_to_int(uint8_t * in, uint64_t size, uint32_t * out);
	 void array_int_to_byte(uint32_t * in, uint64_t size, uint8_t * out);
	 //-----------------------------------------------------------------------------
	 void array_byte_to_long(uint8_t * in, uint64_t size, uint64_t * out);
	 void array_long_to_byte(uint64_t * in, uint64_t size, uint8_t * out);
	 //-----------------------------------------------------------------------------
	 void array_byte_to_float(uint8_t * in, uint64_t size, float * out);
	 void array_float_to_byte(float * in, uint64_t size, uint8_t * out);
	 //-----------------------------------------------------------------------------
	 void array_byte_to_double(uint8_t * in, uint64_t size, double * out);
	 void array_double_to_byte(double * in, uint64_t size, uint8_t * out);
	//-------------------------------------------------------------------------
#endif /* DATA_CONVERSION_H_ */

//-----------------------------------------------------------------------------
