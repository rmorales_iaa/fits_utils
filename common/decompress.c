/*
 * codec.c
 *
 *  Created on: 3 Apr 2019
 *      Author: rafa
 */
//-----------------------------------------------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
//-----------------------------------------------------------------------------
#include "decompress.h"
#include "fitsio.h"
#include "fpack.h"
#include "fpackutil.h"
#include "my_util.h"

//-----------------------------------------------------------------------------
int decompress(my_stream_type * in_stream, my_stream_type * out_stream){

	//#define _REENTRANT

	if ((in_stream == NULL) || (in_stream->data == NULL)){
		printf("\nError in 'decompress'. Input stream or input stream data are null");
		return -500;
	}

	if ((out_stream == NULL) || (out_stream->data == NULL)){
		printf("\nError in 'decompress'. Output stream or input stream data are null");
		return -501;
	}

	fpstate	fpvar;
	memset(&fpvar,0,sizeof(fpstate));
	fp_init (&fpvar);
	my_fp_unpack(in_stream, out_stream, &fpvar);
	return 1;
}
//-----------------------------------------------------------------------------
//End of file "codec.c"
//-----------------------------------------------------------------------------
