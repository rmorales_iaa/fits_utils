/*
 * my_stream.c
 *
 *  Created on: 26 Feb 2019
 *      Author: rafa
 */
//-----------------------------------------------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
//-----------------------------------------------------------------------------
#include "my_stream.h"

//-----------------------------------------------------------------------------
my_stream_type * my_stream_init_with_file(char * s){

	my_stream_type * stream = malloc(sizeof(my_stream_type));
	if (stream == NULL){
		printf("\nError in 'my_stream_init_with_file'. Error reserving memory for the stream peer with file '%s'", s);
		return NULL;
	}
	FILE * f = fopen(s, "rb");
	if (f == NULL){
		printf("\nError in 'my_stream_init_with_file'. Error opening the file '%s'", s);
		return NULL;
	}
	fseek(f,0,SEEK_END);
	size_t size = ftell(f);
	rewind (f);
	if (size < 0){
		fclose(f);
		printf("\nError in 'my_stream_init_with_file'. Error getting the size of file '%s'", s);
		return NULL;
	}

	stream->data = (char *) malloc(size);
	if (stream->data == NULL){
		fclose(f);
		printf("\nError in 'my_stream_init_with_file'. Error reserving memory for the stream peer with file '%s' and file size %lu", s, size);
    	return NULL;
   	}

    size_t read_count = fread(stream->data, 1, size,f);
    if (size != read_count){
    	fclose(f);
    	printf("\nError in 'my_stream_init_with_file'. Error reading from file '%s' and file size %lu. Bytes read: %lu", s, size, read_count);
       	return NULL;
    }

    fclose(f);
    stream->isOpen = 1;
    stream->free_data_when_close = 1;
    stream->pos=0;
    stream->size = size;

    return stream;
}

//-----------------------------------------------------------------------------
my_stream_type * my_stream_init_with_buffer(char * b, size_t b_size){

	my_stream_type * stream = malloc(sizeof(my_stream_type));
	if (stream == NULL){
		printf("\nError in 'my_stream_init_with_buffer'. Error reserving memory for the stream");
		return NULL;
	}

	stream->isOpen = 1;
	stream->free_data_when_close = 0;
	stream->pos = 0;
	stream->size = b_size;
	stream->data = b;  //NO COPYING!!!

    return stream;
}

//-----------------------------------------------------------------------------
void my_stream_close(my_stream_type * stream){

	if (stream == NULL) return;
	if (!stream->isOpen) return;

	if (stream->data != NULL){
		if (stream->free_data_when_close) {
			free(stream->data); //only free when it was reserved
			stream->data = NULL;
		}
	}
	stream->isOpen = 0;
	stream->pos=0;
	stream->size=0;
	free(stream);
}
//-----------------------------------------------------------------------------
size_t my_stream_fread(void *ptr, size_t size, size_t nmemb, FILE *f){

	my_stream_type * stream = (my_stream_type *) f;

	if ((size == 0) || (nmemb == 0)) return 0;

	size_t remain_size = stream->size - stream->pos;
	size_t requested_size = (size * nmemb);
	if (requested_size > remain_size){
		return 0;
	}
	memcpy(ptr, stream->data + stream->pos, requested_size);
	stream->pos += requested_size;
	return nmemb;
}

//-----------------------------------------------------------------------------
size_t my_stream_ftell(FILE *f){

	my_stream_type * stream = (my_stream_type *) f;

	return stream->pos;
}
//-----------------------------------------------------------------------------
//https://stackoverflow.com/questions/27549718/behaviour-of-fseek-and-seek-end
int my_stream_fseek(FILE *f, long int offset, int whence){

	my_stream_type * stream = (my_stream_type *) f;

	if (whence == SEEK_SET){  //beginning
		stream->pos = offset;
	}
	else
		if (whence == SEEK_CUR){ //current
			stream->pos += offset;
		}
		else
			if (whence == SEEK_END){ //end
				stream->pos = stream->size;
				stream->pos += offset;
			}
	return 0;
}

//-----------------------------------------------------------------------------
void my_stream_rewind(FILE * f){

	my_stream_type * stream = (my_stream_type *) f;

	stream->pos = 0;
}
//-----------------------------------------------------------------------------
