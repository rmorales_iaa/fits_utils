//Java Wrapper for sextractor
//https://www.astromatic.net/software/sextractor
//-----------------------------------------------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <sys/stat.h>
#include <dirent.h>
//-----------------------------------------------------------------------------
#include "decompress.h"
#include "my_stream.h"
#include "data_conversion.h"

//-----------------------------------------------------------------------------
//Global vars
//-----------------------------------------------------------------------------

#define CORE_COUNT            12
#define MAX_FILE_LIST       1024
#define FILE_NAME_MAX_SIZE    80

#define SAVE_FITS_FILE 0
//-----------------------------------------------------------------------------
char file_list[MAX_FILE_LIST][FILE_NAME_MAX_SIZE];
int file_list_next_file=0;
int file_list_loaded=0;
//-----------------------------------------------------------------------------
struct timespec start_time;

//-----------------------------------------------------------------------------
void getStartTime(void) {
	clock_gettime(CLOCK_REALTIME, &start_time);
}
//-----------------------------------------------------------------------------
void printElpsedTime(void) {

  struct timespec stop_time;
  clock_gettime(CLOCK_REALTIME, &stop_time);
  if (stop_time.tv_nsec < start_time.tv_nsec) {
	 stop_time.tv_nsec += 1000000000;
	 stop_time.tv_sec--;
  }
  printf("Elpased time %ld.%09ld s\n", (long)(stop_time.tv_sec - start_time.tv_sec),stop_time.tv_nsec - start_time.tv_nsec);
}
//-----------------------------------------------------------------------------
int load_dir_content(char * directory){

	typedef struct __dirstream DIR;
	DIR *dir;

	struct dirent *ent;
	char * filename;
	if ((dir = opendir (directory)) != NULL) {
	  /* print all the files and directories within directory */
	  while ((ent = readdir (dir)) != NULL) {
		  filename = ent->d_name;
		  if(ent->d_type == 8)
			 strcpy(file_list[file_list_loaded++],filename);
	  }
	  closedir (dir);
	  printf("\nLoaded %i files from directory: '%s'" ,file_list_loaded, directory);
	}
	else {
	  printf("\nError loading content of directory: %s" ,directory);
	  return EXIT_FAILURE;
	}
	return 1;
}

//-----------------------------------------------------------------------------

void parse_fits(char * filename){

	printf("\n Parsing file: '%s'", filename);

	//input stream
	my_stream_type * in_stream = my_stream_init_with_file(filename);

	//output stream
    size_t outputByteSize = 360*1024*1204;
	char * output = malloc(outputByteSize);
	if (output == NULL){
		printf("\nError in 'parse_fits'. Error reserving memory for the output");
		return;
	}
	my_stream_type * out_stream = my_stream_init_with_buffer(output, outputByteSize);

	//decompress
	decompress(in_stream, out_stream);

	//save result
	if (SAVE_FITS_FILE){
		FILE * write_ptr = fopen("test.fits","wb");  // w for write, b for binary
    	fwrite(out_stream->data, outputByteSize, 1, write_ptr); // write 10 bytes from our buffer
		fclose(write_ptr);
	}

	my_stream_close(out_stream);
	my_stream_close(in_stream);
	free(output);
}

//-----------------------------------------------------------------------------
void parse_fits_list(char * directory, int max_file_list) {

	pthread_t thread_list[CORE_COUNT];
	char parsing_file_list[MAX_FILE_LIST][FILE_NAME_MAX_SIZE];

	int i=0;
	char * buffer_name;
	for(i=0;i<max_file_list;++i){
		buffer_name = parsing_file_list[i];
		buffer_name[0]=0;
		strcpy(buffer_name,directory);
		strcat(buffer_name,file_list[file_list_next_file+i]);
	}

	for(i=0;i<max_file_list;++i)
		pthread_create(&thread_list[i], NULL, parse_fits, parsing_file_list[i]);

	for(i=0;i<max_file_list;++i)
		pthread_join(thread_list[i], NULL);

	file_list_next_file += max_file_list;
}
//-----------------------------------------------------------------------------
int my_run_thread_raw(char * directory, int max_file_list){

	struct timespec start_time, stop_time;
	clock_gettime(CLOCK_REALTIME, &start_time);

	//load file list
	parse_fits_list(directory, max_file_list);


	clock_gettime(CLOCK_REALTIME, &stop_time);
	if (stop_time.tv_nsec < start_time.tv_nsec) {
		 stop_time.tv_nsec += 1000000000;
		 stop_time.tv_sec--;
	 }

    printf("\nExecution time %ld.%09ld s\n", (long)(stop_time.tv_sec - start_time.tv_sec),stop_time.tv_nsec - start_time.tv_nsec);

	return 1;
}
//-----------------------------------------------------------------------------
int my_run_thread(char * directory){

	struct timespec start_time, stop_time;
	clock_gettime(CLOCK_REALTIME, &start_time);

	load_dir_content(directory);

	int iteration = file_list_loaded / CORE_COUNT;
	int remain =  file_list_loaded % CORE_COUNT;
	int i=0;

	for(i=0;i<iteration;++i)
		my_run_thread_raw(directory, CORE_COUNT);
	my_run_thread_raw(directory, remain);


	clock_gettime(CLOCK_REALTIME, &stop_time);
	if (stop_time.tv_nsec < start_time.tv_nsec) {
		 stop_time.tv_nsec += 1000000000;
		 stop_time.tv_sec--;
	 }
	printf("\n--->TOTAL Execution time %ld.%09ld s\n", (long)(stop_time.tv_sec - start_time.tv_sec),stop_time.tv_nsec - start_time.tv_nsec);

	return 1;
}

//-----------------------------------------------------------------------------
void test_data_type_conversion_short(void){

	uint8_t in[] = {0x01, 0x02, 0x03, 0x04};
	uint16_t out[] = {0xFFFF,0xFFFF};

	array_byte_to_short(in, 2*2, out);

	in[0]=0;
	in[1]=0;
	in[2]=0;
	in[3]=0;

	array_short_to_byte(out, 2 ,in);
}

//-----------------------------------------------------------------------------
void test_data_type_conversion_int(void){

	uint8_t in[] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x6, 0x7, 0x8};
	uint32_t out[] = {0xFFFFFF,0xFFFFFF};

	array_byte_to_int(in, 2*4, out);

	in[0]=0;
	in[1]=0;
	in[2]=0;
	in[3]=0;
	in[4]=0;
	in[5]=0;
	in[6]=0;
	in[7]=0;

	array_int_to_byte(out, 2 ,in);
}

//-----------------------------------------------------------------------------
void test_data_type_conversion_long(void){

	uint8_t in[] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x6, 0x7, 0x8, 0x9, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16};
	uint64_t out[] = {0xFFFFFFFFFF,0xFFFFFFFFFF};

	array_byte_to_long(in, 2*8, out);

	in[0]=0;
	in[1]=0;
	in[2]=0;
	in[3]=0;
	in[4]=0;
	in[5]=0;
	in[6]=0;
	in[7]=0;
	in[8]=0;
	in[9]=0;
	in[10]=0;
	in[11]=0;
	in[12]=0;
	in[13]=0;
	in[14]=0;
	in[15]=0;

	array_long_to_byte(out, 2 ,in);
}

//-----------------------------------------------------------------------------
void test_data_type_conversion_float(void){

	uint8_t in[] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x6, 0x7, 0x8};
	float out[] = {0xFFFFFF,0xFFFFFF};

	array_byte_to_float(in, 2*4, out);

	in[0]=0;
	in[1]=0;
	in[2]=0;
	in[3]=0;
	in[4]=0;
	in[5]=0;
	in[6]=0;
	in[7]=0;

	array_float_to_byte(out, 2 ,in);
}

//-----------------------------------------------------------------------------
void test_data_type_conversion_double(void){

	uint8_t in[] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x6, 0x7, 0x8, 0x9, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16};
	double out[] = {0xFFFFFFFFFF,0xFFFFFFFFFF};

	array_byte_to_double(in, 2*8, out);

	in[0]=0;
	in[1]=0;
	in[2]=0;
	in[3]=0;
	in[4]=0;
	in[5]=0;
	in[6]=0;
	in[7]=0;
	in[8]=0;
	in[9]=0;
	in[10]=0;
	in[11]=0;
	in[12]=0;
	in[13]=0;
	in[14]=0;
	in[15]=0;

	array_double_to_byte(out, 2 ,in);
}
//-----------------------------------------------------------------------------
int main(int argc, char *argv[]){

	getStartTime();
	/*
	test_data_type_conversion_short();
	test_data_type_conversion_int();
	test_data_type_conversion_long();
	test_data_type_conversion_float();
	test_data_type_conversion_double();
*/
	parse_fits("/home/rafa/Downloads/25998.fits.fz");

	//my_run_thread("/home/rafa/jplus/");

	//pix_to_world("/home/rafa/jplus/decompressed/1000001-JPLUS-01491-v2_rSDSS_swp.fits");

	//world_to_pix("/home/rafa/jplus/decompressed/1000001-JPLUS-01491-v2_rSDSS_swp.fits");

	printf("\n End of testing \n");

	printElpsedTime();

	return 1;
}

//-----------------------------------------------------------------------------
//end of file "main.c"
//-----------------------------------------------------------------------------
