//============================================================================
// <Description>     :<Main file for wcstools java wrapper>
// <File>            :<main.cpp>
// <Type>            :<c code>
// <Author>          :<Rafael Morales rmorales@iaa.es>
// <Creation date>   :<04 August 2017>
// <History>         :<None>
//============================================================================

//============================================================================
//Include section
//============================================================================
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
//============================================================================
//System include

//============================================================================
//User include
#include "com_common_fits_util_FitsUtils__.h"
#include "data_conversion.h"
#include "my_stream.h"
//============================================================================
//System include

//============================================================================
//Declaration section
//============================================================================

//============================================================================
//External functions
//============================================================================

extern int decompress(my_stream_type * in_stream, my_stream_type * out_stream);
//----------------------------------------------------------------------------
//============================================================================
//Code section
//============================================================================
JNIEXPORT jint JNICALL Java_com_common_fits_util_FitsUtils_00024_decompress
  (JNIEnv * env
   , jobject obj
   , jbyteArray input_stream
   , jlong      input_stream_byte_size
   , jbyteArray output_stream
   , jlong      output_stream_byte_size){

    char * local_input_stream = (char *) (*env)->GetByteArrayElements(env, input_stream, 0);
    if (local_input_stream == NULL) {
       printf("\n Error in Java_com_common_image_cfitsio_C_1FitsIO_00024_decompress: input_stream");
       return -104;
    }

    char * local_output_stream = (char *) (*env)->GetByteArrayElements(env, output_stream, 0);
    if (local_output_stream == NULL) {
       printf("\n Error in Java_com_common_image_cfitsio_C_1FitsIO_00024_decompress: output_stream");
       return -105;
    }

    //output stream
    my_stream_type * in_stream =  my_stream_init_with_buffer(local_input_stream,  input_stream_byte_size);
    my_stream_type * out_stream = my_stream_init_with_buffer(local_output_stream, output_stream_byte_size);

    int r = decompress(in_stream, out_stream);

    fflush(stdout);
    (*env)->ReleaseByteArrayElements(env, input_stream, (jbyte *)local_input_stream, 0);
    (*env)->ReleaseByteArrayElements(env, output_stream, (jbyte *)local_output_stream, 0);

    return r;
}

//----------------------------------------------------------------------------
JNIEXPORT jint JNICALL Java_com_common_fits_util_FitsUtils_00024_arrayByteToShort
  (JNIEnv * env, jobject obj, jbyteArray input_stream, jlong input_stream_item_size, jshortArray output_stream){

	uint8_t * local_input_stream = (uint8_t *) (*env)->GetByteArrayElements(env, input_stream, 0);
    if (local_input_stream == NULL) {
	  printf("\n Error in Java_com_common_fits_util_FitsUtils_00024_arrayByteToShort: input_stream");
	  return -104;
	}

	uint16_t * local_output_stream = (uint16_t *) (*env)->GetShortArrayElements(env, output_stream, 0);
	if (local_output_stream == NULL) {
	  printf("\n Error in Java_com_common_fits_util_FitsUtils_00024_arrayByteToShort: output_stream ");
	  return -104;
	}

	array_byte_to_short(local_input_stream, input_stream_item_size, local_output_stream);

	fflush(stdout);
	(*env)->ReleaseShortArrayElements(env, output_stream, (jshort *)local_output_stream, 0);
	(*env)->ReleaseByteArrayElements(env,  input_stream,  (jbyte *)local_input_stream, 0);
	return 1;
}

//----------------------------------------------------------------------------
JNIEXPORT jint JNICALL Java_com_common_fits_util_FitsUtils_00024_arrayShortToByte
  (JNIEnv * env, jobject obj, jshortArray input_stream, jlong input_stream_item_size, jbyteArray output_stream){

	uint16_t * local_input_stream = (uint16_t *) (*env)->GetShortArrayElements(env, input_stream, 0);
    if (local_input_stream == NULL) {
	  printf("\n Error in Java_com_common_fits_util_FitsUtils_00024_arrayShortToByte: input_stream");
	  return -104;
	}

    uint8_t * local_output_stream = (uint8_t *) (*env)->GetByteArrayElements(env, output_stream, 0);
	if (local_output_stream == NULL) {
	  printf("\n Error in Java_com_common_fits_util_FitsUtils_00024_arrayShortToByte: output_stream ");
	  return -104;
	}

	array_short_to_byte(local_input_stream, input_stream_item_size, local_output_stream);

	fflush(stdout);
	(*env)->ReleaseByteArrayElements(env,  output_stream, (jbyte *)local_output_stream, 0);
	(*env)->ReleaseShortArrayElements(env, input_stream,  (jshort *)local_input_stream, 0);
	return 1;
}

//----------------------------------------------------------------------------
JNIEXPORT jint JNICALL Java_com_common_fits_util_FitsUtils_00024_arrayByteToInt
  (JNIEnv * env, jobject object, jbyteArray input_stream, jlong input_stream_item_size, jintArray output_stream){

	uint8_t * local_input_stream = (uint8_t *) (*env)->GetByteArrayElements(env, input_stream, 0);
    if (local_input_stream == NULL) {
	  printf("\n Error in Java_com_common_fits_util_FitsUtils_00024_arrayByteToInt: input_stream");
	  return -104;
	}

	uint32_t * local_output_stream = (uint32_t *) (*env)->GetIntArrayElements(env, output_stream, 0);
	if (local_output_stream == NULL) {
	  printf("\n Error in Java_com_common_fits_util_FitsUtils_00024_arrayByteToInt: output_stream ");
	  return -104;
	}

	array_byte_to_int(local_input_stream, input_stream_item_size, local_output_stream);

	fflush(stdout);
	(*env)->ReleaseIntArrayElements(env, output_stream, (jint *)local_output_stream, 0);
	(*env)->ReleaseByteArrayElements(env,  input_stream, (jbyte *)local_input_stream, 0);
	return 1;
}
//----------------------------------------------------------------------------
JNIEXPORT jint JNICALL Java_com_common_fits_util_FitsUtils_00024_arrayIntToByte
  (JNIEnv * env, jobject object, jintArray input_stream, jlong input_stream_item_size, jbyteArray output_stream){

	uint32_t * local_input_stream = (uint32_t *) (*env)->GetIntArrayElements(env, input_stream, 0);
	if (local_input_stream == NULL) {
	  printf("\n Error in Java_com_common_fits_util_FitsUtils_00024_arrayIntToByte: input_stream");
	  return -104;
	}

	uint8_t * local_output_stream = (uint8_t *) (*env)->GetByteArrayElements(env, output_stream, 0);
	if (local_output_stream == NULL) {
	  printf("\n Error in Java_com_common_fits_util_FitsUtils_00024_arrayIntToByte: output_stream ");
	  return -104;
	}

	array_int_to_byte(local_input_stream, input_stream_item_size, local_output_stream);

	fflush(stdout);
	(*env)->ReleaseByteArrayElements(env,  output_stream, (jbyte *)local_output_stream, 0);
	(*env)->ReleaseIntArrayElements(env, input_stream,  (jint *)local_input_stream, 0);
	return 1;
}
//----------------------------------------------------------------------------
JNIEXPORT jint JNICALL Java_com_common_fits_util_FitsUtils_00024_arrayByteToLong
  (JNIEnv * env, jobject object, jbyteArray input_stream, jlong input_stream_item_size, jlongArray output_stream){

	uint8_t * local_input_stream = (uint8_t *) (*env)->GetByteArrayElements(env, input_stream, 0);
    if (local_input_stream == NULL) {
	  printf("\n Error in Java_com_common_fits_util_FitsUtils_00024_arrayByteToLong: input_stream");
	  return -104;
	}

	uint64_t * local_output_stream = (uint64_t *) (*env)->GetLongArrayElements(env, output_stream, 0);
	if (local_output_stream == NULL) {
	  printf("\n Error in Java_com_common_fits_util_FitsUtils_00024_arrayByteToLong: output_stream ");
	  return -104;
	}

	array_byte_to_long(local_input_stream, input_stream_item_size, local_output_stream);

	fflush(stdout);
	(*env)->ReleaseIntArrayElements(env, output_stream, (jint *)local_output_stream, 0);
	(*env)->ReleaseByteArrayElements(env,  input_stream, (jbyte *)local_input_stream, 0);
	return 1;
}

//----------------------------------------------------------------------------
JNIEXPORT jint JNICALL Java_com_common_fits_util_FitsUtils_00024_arrayLongToByte
  (JNIEnv * env, jobject object , jlongArray input_stream, jlong input_stream_item_size, jbyteArray output_stream){

	uint64_t * local_input_stream = (uint64_t *) (*env)->GetLongArrayElements(env, input_stream, 0);
	if (local_input_stream == NULL) {
	  printf("\n Error in Java_com_common_fits_util_FitsUtils_00024_arrayLongToByte: input_stream");
	  return -104;
	}

	uint8_t * local_output_stream = (uint8_t *) (*env)->GetByteArrayElements(env, output_stream, 0);
	if (local_output_stream == NULL) {
	  printf("\n Error in Java_com_common_fits_util_FitsUtils_00024_arrayLongToByte: output_stream ");
	  return -104;
	}

	array_long_to_byte(local_input_stream, input_stream_item_size, local_output_stream);

	fflush(stdout);
	(*env)->ReleaseByteArrayElements(env,  output_stream, (jbyte *)local_output_stream, 0);
	(*env)->ReleaseLongArrayElements(env, input_stream,  (jlong *)local_input_stream, 0);
	return 1;
}
//----------------------------------------------------------------------------
JNIEXPORT jint JNICALL Java_com_common_fits_util_FitsUtils_00024_arrayByteToFloat
  (JNIEnv * env, jobject object, jbyteArray input_stream, jlong input_stream_item_size, jfloatArray output_stream){

	uint8_t * local_input_stream = (uint8_t *) (*env)->GetByteArrayElements(env, input_stream, 0);
    if (local_input_stream == NULL) {
	  printf("\n Error in Java_com_common_fits_util_FitsUtils_00024_arrayByteToFloat: input_stream");
	  return -104;
	}

    float * local_output_stream = (float *) (*env)->GetFloatArrayElements(env, output_stream, 0);
	if (local_output_stream == NULL) {
	  printf("\n Error in Java_com_common_fits_util_FitsUtils_00024_arrayByteToFloat: output_stream ");
	  return -104;
	}

	array_byte_to_float(local_input_stream, input_stream_item_size, local_output_stream);

	fflush(stdout);
	(*env)->ReleaseFloatArrayElements(env, output_stream, (jfloat *)local_output_stream, 0);
	(*env)->ReleaseByteArrayElements(env,  input_stream,  (jbyte *)local_input_stream, 0);
	return 1;

}
//----------------------------------------------------------------------------
JNIEXPORT jint JNICALL Java_com_common_fits_util_FitsUtils_00024_arrayFloatToByte
  (JNIEnv * env, jobject object, jfloatArray input_stream, jlong input_stream_item_size, jbyteArray output_stream){

	float * local_input_stream = (float *) (*env)->GetFloatArrayElements(env, input_stream, 0);
	if (local_input_stream == NULL) {
	  printf("\n Error in Java_com_common_fits_util_FitsUtils_00024_arrayFloatToByte: input_stream");
	  return -104;
	}

	uint8_t * local_output_stream = (uint8_t *) (*env)->GetByteArrayElements(env, output_stream, 0);
	if (local_output_stream == NULL) {
	  printf("\n Error in Java_com_common_fits_util_FitsUtils_00024_arrayFloatToByte: output_stream ");
	  return -104;
	}

	array_float_to_byte(local_input_stream, input_stream_item_size, local_output_stream);

	fflush(stdout);
	(*env)->ReleaseByteArrayElements(env,  output_stream, (jbyte *)local_output_stream, 0);
	(*env)->ReleaseFloatArrayElements(env, input_stream,  (jfloat *)local_input_stream, 0);
	return 1;
}
//----------------------------------------------------------------------------
JNIEXPORT jint JNICALL Java_com_common_fits_util_FitsUtils_00024_arrayByteToDouble
  (JNIEnv * env, jobject object, jbyteArray input_stream, jlong input_stream_item_size, jdoubleArray output_stream){

	uint8_t * local_input_stream = (uint8_t *) (*env)->GetByteArrayElements(env, input_stream, 0);
    if (local_input_stream == NULL) {
	  printf("\n Error in Java_com_common_fits_util_FitsUtils_00024_arrayByteToDouble: input_stream");
	  return -104;
	}

	double * local_output_stream = (double *) (*env)->GetDoubleArrayElements(env, output_stream, 0);
	if (local_output_stream == NULL) {
	  printf("\n Error in Java_com_common_fits_util_FitsUtils_00024_arrayByteToDouble: output_stream ");
	  return -104;
	}

	array_byte_to_double(local_input_stream, input_stream_item_size, local_output_stream);

	fflush(stdout);
	(*env)->ReleaseDoubleArrayElements(env, output_stream, (jdouble *)local_output_stream, 0);
	(*env)->ReleaseByteArrayElements(env,  input_stream, (jbyte *)local_input_stream, 0);
	return 1;
}

//----------------------------------------------------------------------------
JNIEXPORT jint JNICALL Java_com_common_fits_util_FitsUtils_00024_arrayDoubleToByte
  (JNIEnv * env, jobject object, jdoubleArray input_stream, jlong input_stream_item_size, jbyteArray output_stream){

	double * local_input_stream = (double *) (*env)->GetDoubleArrayElements(env, input_stream, 0);
	if (local_input_stream == NULL) {
	  printf("\n Error in Java_com_common_fits_util_FitsUtils_00024_arrayDoubleToByte: input_stream");
	  return -104;
	}

	uint8_t * local_output_stream = (uint8_t *) (*env)->GetByteArrayElements(env, output_stream, 0);
	if (local_output_stream == NULL) {
	  printf("\n Error in Java_com_common_fits_util_FitsUtils_00024_arrayDoubleToByte: output_stream ");
	  return -104;
	}

	array_double_to_byte(local_input_stream, input_stream_item_size, local_output_stream);

	fflush(stdout);
	(*env)->ReleaseByteArrayElements(env,  output_stream, (jbyte *)local_output_stream, 0);
	(*env)->ReleaseDoubleArrayElements(env, input_stream,  (jdouble *)local_input_stream, 0);
	return 1;
}
//----------------------------------------------------------------------------

//============================================================================
//============================================================================

//============================================================================
//End of file:main.c
//============================================================================
