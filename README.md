C wrapper (used in Scala/Java algorithms) for cfitsio library.
CFITSIO (https://heasarc.gsfc.nasa.gov/fitsio/) is a library of C and Fortran subroutines for reading and writing data files in FITS (Flexible Image Transport System) data format
